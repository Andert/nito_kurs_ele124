﻿using System;

namespace ConsoleApplication2
{
    internal class Person
    {
        private string navn;
        private int alder;
        private Hund hund;

        public Person(string navn, int alder)
        {
            this.navn = navn;
            this.alder = alder;
        }

        internal bool giHund(Hund enHund)
        {
            if (this.hund == null)
            {
                this.hund = enHund;
                return true;
            }
            else return false;
        }

        internal void giVekkHund(Person annenPerson)
        {
            if (this.hund != null)
            {
                if (annenPerson.giHund(this.hund))
                {
                    this.hund = null;
                }
            }
        }

        public override string ToString()
        {
            return "Dette er " + this.navn + "\n Hans hund er" + this.hund;
        }
    }
}