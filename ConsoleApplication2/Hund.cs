﻿namespace ConsoleApplication2
{
    internal class Hund
    {
        private string navn;

        public Hund(string navn)
        {
            this.navn = navn;
        }

        public override string ToString()
        {
            return this.navn;
        }
    }
}