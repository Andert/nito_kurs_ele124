﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Person meg = new Person ("Anders",25);
            Hund enHund = new Hund ("Fido");
            Person deg = new Person("Erik", 23);

            Console.WriteLine(meg);
            Console.WriteLine(deg);


            Console.WriteLine("\n Gir Anders en Hund");
            meg.giHund(enHund);
            Console.WriteLine(meg);
            Console.WriteLine(deg);

            Console.WriteLine("\n Anders gir fra seg sin Hund");
            meg.giVekkHund(deg);
            Console.WriteLine(meg);
            Console.WriteLine(deg);

            Console.WriteLine("\n Erik gir tilbake Fido :)");
            deg.giVekkHund(meg);
            Console.WriteLine(meg);
            Console.WriteLine(deg);

            Console.ReadKey();
        }
    }
}
