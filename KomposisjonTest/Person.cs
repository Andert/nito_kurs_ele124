﻿namespace KomposisjonTest
{
    internal class Person
    {
        private string navn;
        private int alder;
        private HemmeligPassord passord;

        public Person(string navn, int alder, string passord)
        {
            this.navn = navn;
            this.alder = alder;
            this.passord = new HemmeligPassord(passord);
        }
    }
}