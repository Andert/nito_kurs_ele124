﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            while (true)
            {
                try
                {
                    a = int.Parse(Console.ReadLine());
                    b = int.Parse(Console.ReadLine());
                    int ans = a / b;
                    Console.WriteLine(ans);
                }
                catch (FormatException formatEx)
                {
                    Console.WriteLine("Du har skrevet inn ugyldig tall\n" + formatEx.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("En feil har skjedd\n" + ex.Message);
                }
                finally
                {
                    Console.WriteLine("Denne meldingen vil vise om du har exception eller ei");
                }
            }

        }
    }
}
