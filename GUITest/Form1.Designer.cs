﻿namespace GUITest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxAnsatt = new System.Windows.Forms.ListBox();
            this.listBoxOrdre = new System.Windows.Forms.ListBox();
            this.buttonNewAnsatt = new System.Windows.Forms.Button();
            this.buttonNewOrdre = new System.Windows.Forms.Button();
            this.textBoxOrdreNavn = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listBoxAnsatt
            // 
            this.listBoxAnsatt.FormattingEnabled = true;
            this.listBoxAnsatt.Location = new System.Drawing.Point(13, 13);
            this.listBoxAnsatt.Name = "listBoxAnsatt";
            this.listBoxAnsatt.Size = new System.Drawing.Size(107, 277);
            this.listBoxAnsatt.TabIndex = 0;
            this.listBoxAnsatt.SelectedIndexChanged += new System.EventHandler(this.listBoxAnsatt_SelectedIndexChanged);
            // 
            // listBoxOrdre
            // 
            this.listBoxOrdre.FormattingEnabled = true;
            this.listBoxOrdre.Location = new System.Drawing.Point(126, 12);
            this.listBoxOrdre.Name = "listBoxOrdre";
            this.listBoxOrdre.Size = new System.Drawing.Size(107, 277);
            this.listBoxOrdre.TabIndex = 0;
            // 
            // buttonNewAnsatt
            // 
            this.buttonNewAnsatt.Location = new System.Drawing.Point(13, 297);
            this.buttonNewAnsatt.Name = "buttonNewAnsatt";
            this.buttonNewAnsatt.Size = new System.Drawing.Size(107, 23);
            this.buttonNewAnsatt.TabIndex = 1;
            this.buttonNewAnsatt.Text = "Ny ansatt";
            this.buttonNewAnsatt.UseVisualStyleBackColor = true;
            this.buttonNewAnsatt.Click += new System.EventHandler(this.buttonNewAnsatt_Click);
            // 
            // buttonNewOrdre
            // 
            this.buttonNewOrdre.Location = new System.Drawing.Point(126, 326);
            this.buttonNewOrdre.Name = "buttonNewOrdre";
            this.buttonNewOrdre.Size = new System.Drawing.Size(107, 23);
            this.buttonNewOrdre.TabIndex = 1;
            this.buttonNewOrdre.Text = "Ny ordre";
            this.buttonNewOrdre.UseVisualStyleBackColor = true;
            this.buttonNewOrdre.Click += new System.EventHandler(this.buttonNewOrdre_Click);
            // 
            // textBoxOrdreNavn
            // 
            this.textBoxOrdreNavn.Location = new System.Drawing.Point(126, 295);
            this.textBoxOrdreNavn.Name = "textBoxOrdreNavn";
            this.textBoxOrdreNavn.Size = new System.Drawing.Size(107, 20);
            this.textBoxOrdreNavn.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 532);
            this.Controls.Add(this.textBoxOrdreNavn);
            this.Controls.Add(this.buttonNewOrdre);
            this.Controls.Add(this.buttonNewAnsatt);
            this.Controls.Add(this.listBoxOrdre);
            this.Controls.Add(this.listBoxAnsatt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonNewAnsatt;
        private System.Windows.Forms.Button buttonNewOrdre;
        public System.Windows.Forms.ListBox listBoxAnsatt;
        private System.Windows.Forms.TextBox textBoxOrdreNavn;
        public System.Windows.Forms.ListBox listBoxOrdre;
    }
}

