﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUITest
{
    public partial class Form1 : Form
    {
        private List<Ansatt> ansattListe;
        private Ansatt markertAnsatt;

        public Form1()
        {
            InitializeComponent();
            AnsattListe = new List<Ansatt>();
            AnsattListe.Add(new Ansatt("Anders", "Lederavdelig"));
            AnsattListe.Add(new Ansatt("Marius", "Vaktmester"));
            listBoxAnsatt.DataSource = AnsattListe;
        }

        public List<Ansatt> AnsattListe
        {
            get
            {
                return ansattListe;
            }

            set
            {
                ansattListe = value;
            }
        }
        

        private void buttonNewAnsatt_Click(object sender, EventArgs e)
        {
            FormNewAnsatt formNewAnsatt = new FormNewAnsatt(this);
            formNewAnsatt.Show();
        }

        private void listBoxAnsatt_SelectedIndexChanged(object sender, EventArgs e)
        {
            Ansatt selectedAnsattTemp = (Ansatt)listBoxAnsatt.SelectedItem;
            if (selectedAnsattTemp != null)
            {
                this.markertAnsatt = selectedAnsattTemp;
                listBoxOrdre.DataSource = null;
                listBoxOrdre.DataSource = this.markertAnsatt.Ordre;
            }
        }

        private void buttonNewOrdre_Click(object sender, EventArgs e)
        {
            this.markertAnsatt.Ordre.Add(new Ordre(textBoxOrdreNavn.Text, 5));
            listBoxOrdre.DataSource = null;
            listBoxOrdre.DataSource = this.markertAnsatt.Ordre;
        }
    }
}
