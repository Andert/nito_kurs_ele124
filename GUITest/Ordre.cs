﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUITest
{
    class Ordre
    {
        private string produktNavn;
        private int antall;

        public string ProduktNavn
        {
            get
            {
                return produktNavn;
            }

            set
            {
                produktNavn = value;
            }
        }

        public int Antall
        {
            get
            {
                return antall;
            }

            set
            {
                antall = value;
            }
        }

        public Ordre(string produktNavn, int antall)
        {
            this.ProduktNavn = produktNavn;
            this.Antall = antall;

        }

        public override string ToString()
        {
            return this.produktNavn + "\t : " + this.antall;
        }
    }
}
