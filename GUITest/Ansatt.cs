﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUITest
{
    public class Ansatt
    {
        private string navn;
        private string avdeling;
        private List<Ordre> ordre;

        public string Navn
        {
            get
            {
                return navn;
            }

            set
            {
                navn = value;
            }
        }

        public string Avdeling
        {
            get
            {
                return avdeling;
            }

            set
            {
                avdeling = value;
            }
        }

        internal List<Ordre> Ordre
        {
            get
            {
                return ordre;
            }

            set
            {
                ordre = value;
            }
        }

        public Ansatt(string navn, string avdeling)
        {
            this.Navn = navn;
            this.Avdeling = avdeling;
            Ordre = new List<Ordre>();
        }

        public override string ToString()
        {
            return this.navn + " : " + this.avdeling;
        }
    }
}
