﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUITest
{
    public partial class FormNewAnsatt : Form
    {
        private Form1 parentForm;

        public FormNewAnsatt(Form1 parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            parentForm.AnsattListe.Add(new Ansatt(textBoxNyttNavn.Text, textBoxNyAvdeling.Text));
            parentForm.listBoxAnsatt.DataSource = null;
            parentForm.listBoxAnsatt.DataSource = parentForm.AnsattListe;
            this.Close();
        }
    }
}
