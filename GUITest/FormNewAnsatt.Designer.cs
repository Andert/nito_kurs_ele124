﻿namespace GUITest
{
    partial class FormNewAnsatt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNyttNavn = new System.Windows.Forms.TextBox();
            this.textBoxNyAvdeling = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxNyttNavn
            // 
            this.textBoxNyttNavn.Location = new System.Drawing.Point(12, 12);
            this.textBoxNyttNavn.Name = "textBoxNyttNavn";
            this.textBoxNyttNavn.Size = new System.Drawing.Size(100, 20);
            this.textBoxNyttNavn.TabIndex = 0;
            // 
            // textBoxNyAvdeling
            // 
            this.textBoxNyAvdeling.Location = new System.Drawing.Point(12, 38);
            this.textBoxNyAvdeling.Name = "textBoxNyAvdeling";
            this.textBoxNyAvdeling.Size = new System.Drawing.Size(100, 20);
            this.textBoxNyAvdeling.TabIndex = 0;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(12, 390);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 23);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // FormNewAnsatt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 425);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxNyAvdeling);
            this.Controls.Add(this.textBoxNyttNavn);
            this.Name = "FormNewAnsatt";
            this.Text = "FormNewAnsatt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNyttNavn;
        private System.Windows.Forms.TextBox textBoxNyAvdeling;
        private System.Windows.Forms.Button buttonOK;
    }
}