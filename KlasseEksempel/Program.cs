﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlasseEksempel
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestHund();

            int[] enTabell = new int[4];
            enTabell[0] = 7;
            enTabell[1] = 3;
            enTabell[2] = 5;
            enTabell[3] = 2;

            List<int> enList = new List<int>();
            enList.Add(5);
            enList.Add(8);
            enList.Add(8);
            enList.Add(2);

            Console.WriteLine("Inneholder listen 4?" + enList.Contains(4));
            Console.WriteLine("Hvilken index er 8 i listen?" + enList.IndexOf(8));
            enList.Sort();

            for (int i = 0; i < enList.Count; i++)
            {
                Console.WriteLine(enList[i]);
            }

            Console.ReadKey();
        }

        private static void TestHund()
        {
            Hund minHund = new Hund("Fido", 12);
            Console.WriteLine(minHund.ToString());
            Console.WriteLine("hundens alder er: " + minHund.Alder);
        }
    }
}
