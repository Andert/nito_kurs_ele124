﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlasseEksempel
{
    class Hund
    {
        private string navn;

        public string Navn
        {
            get { return navn; }
            private set
            {
                if (value.Length < 10)
                {
                    navn = value;
                }
            }
        }


        private int alder;

        public int Alder
        {
            get { return alder; }
            set { alder = value; }
        }


        public Hund(string innNavn, int innAlder)
        {
            this.navn = innNavn;
            this.alder = innAlder;
        }

        public override string ToString()
        {
            return "Navnet på hunden er: "+this.navn+"\n og alderen er: "+this.alder;
        }
    }
}
