﻿namespace ArvTest
{
    internal class Dyr
    {
        private string navn;
        private int alder;

        public string Navn
        {
            get
            {
                return navn;
            }

            set
            {
                navn = value;
            }
        }

        public int Alder
        {
            get
            {
                return alder;
            }

            set
            {
                alder = value;
            }
        }

        public Dyr(string navn, int alder)
        {
            this.Navn = navn;
            this.Alder = alder;
        }
    }
}