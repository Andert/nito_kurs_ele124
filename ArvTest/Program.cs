﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArvTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Fisk minFisk = new Fisk("Guppi", 2);
            Katt minKatt = new Katt("Pusur", 12, "tråd", "Pusur er glad i mat");
            Fisk dinFisk = new Fisk("lilleFisk", 1);
            Katt dinKatt = new Katt("kattePus", 15, "ball", "Liker ikke vann");

            List<Dyr> listeMedKDyr = new List<Dyr>();
            listeMedKDyr.Add(minFisk);
            listeMedKDyr.Add(minKatt);
            listeMedKDyr.Add(dinFisk);
            listeMedKDyr.Add(dinKatt);

            int maxAlder = FinnEldsteAlder(listeMedKDyr);

            //Console.WriteLine(maxAlder);

            Console.WriteLine(minKatt);
            Console.WriteLine(dinKatt);

            //Fisk.lagFiskeLyder();

            Console.ReadKey();
        }

        private static int FinnEldsteAlder(List<Dyr> listeMedKDyr)
        {
            int maxAlder = 0;
            for (int i = 0; i < listeMedKDyr.Count; i++)
            {
                if (maxAlder < listeMedKDyr[i].Alder) maxAlder = listeMedKDyr[i].Alder;
            }

            return maxAlder;
        }
    }
}
