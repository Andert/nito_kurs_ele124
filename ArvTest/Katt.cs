﻿namespace ArvTest
{
    internal class Katt : Dyr
    {
        private string leke;

        private string personligInfo;
        static public string katteInfo = "Alle katter er søte";

        static public int iDCount = 0;
        private int ID;

        public Katt(string navn, int alder, string leke, string personligInfo) : base(navn, alder)
        {
            this.leke = leke;
            this.personligInfo = personligInfo;

            this.ID = iDCount;
            iDCount++;
        }

        public override string ToString()
        {
            return "Generell info " + katteInfo + "\n"
                + this.Navn + " er " + this.Alder + " år gammel\n"
                + "Litt info om DENNE katten: " + this.personligInfo
                +"\n ID: "+ID;
        }
    }
}